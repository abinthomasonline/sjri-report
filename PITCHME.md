## Recommendation system for diet software using online machine learning 

Abin Thomas  
Intern, SJRI 

---

### Content Based Recommendation

* All foods can be represented universally as n-dimensional vector, where n is the number of nutrients.

* **`cosine similarity`** can be used to measure the similarity between two foods.

---

### Association Rule Mining

* To find the rules of co-occurance of foods given a set of meals.

* **`support`** - measure of occurance of a combination of food.

* **`rule`** - if (food1, food2, ...) then (food3, food4, ...)

* **`confidence`** - measure of strength of a rule.

---

### ARM - Cont'd...

* `support(A) = (no of occur(A)/total no of meals)`

* `confidence(A->B) = support(A+B)/support(A)`

---

### ARM - Apriori Algorithm

* Optimised to keep minimum combinations in memory.

* Novel methods for inserting new food to the system.

---

### Online Machine Learning

* Method of machine learning in which data is available in sequential order.

* Used when it is computationally infeasible to train over the entire data.

* Used when dynamic adaptation to new patterns in data is required. 

---

### Online Association Rule Mining - Christian Hidber

* Inserting new foods and combinations to the system.

* Updating the support values.

* Removing foods and combinations with minimum support.

---

### Reinforced Association Rule Mining

* Reward and Punishment method to update **support**.

---

### Where ?

* User Input

* Input to Nutrient Optimization

* Filter out supplements

---

### Possible Improvements

* Efficient Data Collection

* Dictinction of meals (BreakFast/Lunch/Dinner).

* Hierarchical model country states and disctricts

---

### Thank You